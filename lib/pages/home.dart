import 'package:flutter/material.dart';

import '../widgets/TitledParagraphCard.dart';
import '../widgets/FlowDrawer.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Home"),
      ),
      endDrawer: new FlowDrawer(),
      body: new Container(
        margin: EdgeInsets.symmetric(horizontal: 10.0),
        child: new ListView(
          children: <Widget>[
            new TitledParagraphCard(
              "The Flow Solution",
              [
                "Using the latest technology, Flow delivers a continuously evolving learning and development solution tailored to the hospitality and tourism sector, providing accredited, certificated training throughout the industry.",
                "Easily tailored to fit into your business, Flow will have a significant impact on productivity and profitability by developing and motivating staff, reducing training costs and increasing sales. ",
                "Flow has been designed to inspire learning through our comprehensive portfolio of innovative and engaging online modules. We also deliver control and visibility of all learning and development activity through our versatile and intuitive management system, The Flow Zone.",
                "And with over 30 years of hospitality experience, we feel our industry expertise gives us unique empathy with all our clients, enabling an excellent level of service and support."
              ]
            ),
          ],
        ),
      ),
    );
  }
}
