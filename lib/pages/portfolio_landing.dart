import 'package:flutter/material.dart';

import '../widgets/FlowDrawer.dart';
import '../widgets/TitledParagraphCard.dart';
import '../widgets/CertificationsExpansionTile.dart';
import '../pages/portfolio_modules.dart';

class PortfolioLanding extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
        length: 2,
        child: new Scaffold(
          appBar: new AppBar(
            title: new Text("Our Portfolio/Modules"),
            bottom: new TabBar(
              tabs: <Widget>[
                new Tab(
                  text: "Info",
                ),
                new Tab(
                    text: "Certifications"
                )
              ],
            ),
          ),
          endDrawer: new FlowDrawer(),
          body: new TabBarView(
            children: <Widget>[
              new Container(
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  child: new ListView(
                    children: <Widget>[
                      new TitledParagraphCard(
                          "Our Portfolio / Modules",
                          [
                            "Individually designed to the highest quality, all our modules are accredited by the People 1st Quality Mark, the Institute of Hospitality, CPD  and SQA. Our Food Safety modules are also accredited by IQ, Health and Safety has been approved by RoSPA and our licensing modules are accredited by SQA.",
                            "Each module hosts a stimulating variety of interactions, games and activities designed to appeal to all learning styles. The modules can be accessed through all modern browsers, tablet devices and they are mobile friendly too.",
                            "Learning is validated throughout the module by regular ‘pass to progress’ testing and, on successful completion of a final exam, the learner receives an endorsed certificate of achievement. ",
                            "In order to ensure that your staff get the best from their learning experience each module also contains downloadable PDFs for further study and future reference."
                          ]
                      ),
                      new RaisedButton(
                        child: new Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text("Check out our Modules", textScaleFactor: 1.1,),
                            new Icon(Icons.arrow_right, )
                          ],
                        ),
                        onPressed: () {
                          Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context) => new PortfolioModules()));
                        },
                      ),
                      new SizedBox( // bit of a spacer
                        height: 10.0,
                      )
                    ],
                  )
              ),
              new Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                child: new ListView(
                  children: <Widget>[
                    new CertificationsExpansionTile(),
                  ],
                )
              ),
            ],
          ),
        )
    );
  }
}
