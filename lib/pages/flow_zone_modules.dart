import 'package:flutter/material.dart';

import '../widgets/FlowDrawer.dart';
import '../widgets/TitledParagraphCard.dart';

class FlowZoneModules extends StatelessWidget {

  List<Map<String, dynamic>> _moduleData = [
    {
      "title": "Training Manager",
      "paragraphs": [
        "The Training Manager provides real-time visibility across your business in the blink of an eye.  From small independents to large organisations, the Training Manager provides the ability to issue all types of training, monitor progress and track results. ",
        "Our team will help with the setup of your company and training structures to ensure your staff can be allocated the right online training, classroom based courses and on the job competencies. ",
        "All training can be automatically issued to new recruits or recent promotees with associated due dates if needed. Managers can track the progress of their staff which ensures that the right people get to see the right data at the right time. "
      ]
    },
    {
      "title": "Talent Manager",
      "paragraphs": [
        "The Talent Manager is designed to help your rising stars achieve the recognition and opportunities they deserve. It inspires career development by bringing visibility to all learning activity, career progression and performance management.",
        "The Talent Manager allows you to configure career paths for your staff providing them with clear visibility of how their career can progress within your business. Your staff can easily see what online modules, classroom based training and competencies are required for them to progress to the next level.",
        "Our intuitive appraisals system supports managers across your business and ensures that the right conversations and feedback are provided when needed."
      ]
    },
    {
      "title": "Newsreel & Noticeboard",
      "paragraphs": [
        "Good communication is so important for all businesses no matter their size. Flow has created the Newsreel and Noticeboard tools that allow you to send a variety of instant alerts across your organisation.",
        "Staff updates, useful video links, details of upcoming events, company vacancies, new menu launches, staff handbooks and company policies are just some examples of the type of alerts that our customers send to their staff. The alerts are trackable too so you can see what is being read, by who and when."
      ]
    },
    {
      "title": "Reporting Suite",
      "paragraphs": [
        "Ensuring staff compliance and tracking training completion rates across the business is more and more important these days. The Reporting Suite can provide high level overviews of all training across the entire business. It also allows you to drill down and review performance at individual locations or even specific trainees.",
        "All our reports are real time so you have the most up to date information available to you. The reports can also be exported to Excel allowing you to save important reports and analyse further in your own time."
      ]
    }
  ];

  List<Widget> _generateTabs() {
    List<Widget> children = [];

    this._moduleData.forEach((item) {
      children.add(
        new Tab(
          text: item["title"],
        )
      );
    });

    return children;
  }

  List<Widget> _generateTabBarViewChildren() {
    List<Widget> children = [];

    this._moduleData.forEach((item) {
      children.add(
        new Container(
          margin: EdgeInsets.symmetric(horizontal: 10.0),
          child: new ListView(
            children: <Widget>[
              new TitledParagraphCard(
                  "The "+item['title'],
                  item['paragraphs']
              ),
            ],
          )
        )
      );
    });

    return children;
  }

  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: this._moduleData.length,
      child: new Scaffold(
        appBar: new AppBar(
          title: new Text("Flow Zone"),
          bottom: new TabBar(
            tabs: this._generateTabs(),
            isScrollable: true,
          ),
        ),
        endDrawer: new FlowDrawer(),
        body: new TabBarView(
          children: this._generateTabBarViewChildren(),
        ),
      ),
    );
  }
}
