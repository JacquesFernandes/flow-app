import 'package:flutter/material.dart';

import '../widgets/FlowDrawer.dart';
import '../widgets/TitledParagraphCard.dart';
import './flow_zone_modules.dart';

class FlowZoneLanding extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Flow Zone"),
      ),
      endDrawer: new FlowDrawer(),
      body: new Container(
        margin: EdgeInsets.symmetric(horizontal: 10.0),
        child: new ListView(
          children: <Widget>[
            new TitledParagraphCard(
              "The Flow Zone",
              [
                "Using our versatile management system, The Flow Zone, you can use the Training Manager to issue training, monitor progress, view results and keep up to date with all learning activity in your business. Not only Flow modules but all types of learning activity such as workshops, on-the-job coaching, other online modules and competences can be recorded and monitored within The Flow Zone.",
                "The Talent Manager includes career planning and appraisal functionality helping you manage career progression and succession planning.",
                "Using the Newsreel and Noticeboard you can target and track communication with people at all levels within your business ensuring everyone is well informed and up to date.",
                "The Reporting Suite provides access to a whole range of reports that provide visibility across your organisation."
              ]
            ),
            new RaisedButton(
              child: new Text("Interested? Click here."),
              onPressed: () {
                Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context) => new FlowZoneModules()));
              },
            )
          ],
        ),
      ),
    );
  }
}
