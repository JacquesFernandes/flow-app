import 'package:flutter/material.dart';

import '../widgets/FlowDrawer.dart';
import '../widgets/PortfolioModuleSearchList.dart';

class PortfolioModuleSearch extends StatefulWidget {

  @override
  _PortfolioModuleSearchState createState() => _PortfolioModuleSearchState();
}

class _PortfolioModuleSearchState extends State<PortfolioModuleSearch> {

  String searchString="";
  TextEditingController searchController = new TextEditingController();

  void search(String searchString) {
    this.setState(() {
      this.searchString = searchString;
      if(searchString.isEmpty) {
        this.searchController.clear();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Search Modules"),
      ),
      endDrawer: new FlowDrawer(),
      body: new Container(
        margin: EdgeInsets.symmetric(horizontal: 10.0),
        child: new Column(
          children: <Widget>[
            new TextField(
              decoration: new InputDecoration(
                labelText: "Module Search",
                suffixIcon: new IconButton(icon: new Icon(Icons.clear), onPressed: () =>this.search(""))
              ),
              onChanged: this.search,
              controller: this.searchController,
            ),
            new Expanded(
              child: new PortfolioModuleSearchList(
                searchString: this.searchString,
              )
            )
          ],
        ),
      ),
    );
  }
}
