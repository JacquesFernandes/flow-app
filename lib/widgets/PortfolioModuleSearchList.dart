import 'package:flutter/material.dart';

import '../widgets/LandscapeWebView.dart';

class PortfolioModuleSearchList extends StatelessWidget {

  final String searchString;
  final List<Module> modules = [
    new Module(
      mainModuleName: "Compliance Suite",
      subModuleName: "Food Safety",
      descriptionParagraphs: [
        "Understand the different type of bacteria, the different types of food poisoning and how food can become contaminated",
        "Know how to store and prepare food by following safe working practices",
        "Understand the importance of working in a safe, hygienic and healthy work place",
        "Be familiar with 7 key principles of the HACCP food management systems (Level 2 only)",
        "Complete an industry endorsed module "
      ],
      demoUrl: "https://www.flowmodulemanager.co.uk/demonstration/food_safety/?cookie_consent=true&autoopen=true",
    ),
    new Module(
      mainModuleName: "Compliance Suite",
      subModuleName: "Licensing",
      descriptionParagraphs: [],
      demoUrl: "https://www.flowmodulemanager.co.uk/demonstration/licensing_demo/?cookie_consent=true&autoopen=true",
    ),
    new Module(
      mainModuleName: "Compliance Suite",
      subModuleName: "Equality & Diversity",
      descriptionParagraphs: [],
      demoUrl: "https://www.flowmodulemanager.co.uk/demonstration/equality_diversity/?cookie_consent=true&autoopen=true",
    ),
    new Module(
      mainModuleName: "Compliance Suite",
      subModuleName: "Working in a Kitchen",
      descriptionParagraphs: [],
      demoUrl: "https://www.flowmodulemanager.co.uk/demonstration/working-kitchen/?cookie_consent=true&autoopen=true",
    ),
    new Module(
      mainModuleName: "Compliance Suite",
      subModuleName: "Food Allergens",
      descriptionParagraphs: [],
      demoUrl: "https://www.flowmodulemanager.co.uk/demonstration/allergens/?cookie_consent=true&autoopen=true",
    ),
    new Module(
      mainModuleName: "Compliance Suite",
      subModuleName: "PCI Data Security Standards",
      descriptionParagraphs: [],
      demoUrl: "https://www.flowmodulemanager.co.uk/demonstration/pci/?cookie_consent=true&autoopen=true",
    ),
    new Module(
      mainModuleName: "Service Suite",
      subModuleName: "Wine",
      descriptionParagraphs: [
        "Understand the different wine styles that are produced",
        "Know how to taste wine and identify its key characteristics",
        "Be aware of how to present and serve wine to your customers",
        "Be able to pair wine with food"
      ],
      demoUrl: "https://www.flowmodulemanager.co.uk/demonstration/wine/?cookie_consent=true&autoopen=true",
    ),
    new Module(
      mainModuleName: "Service Suite",
      subModuleName: "Cocktails",
      descriptionParagraphs: [],
      demoUrl: "https://www.flowmodulemanager.co.uk/demonstration/creating_cocktails/?cookie_consent=true&autoopen=true",
    ),
    new Module(
      mainModuleName: "Service Suite",
      subModuleName: "Cask Ale",
      descriptionParagraphs: [],
      demoUrl: "https://www.flowmodulemanager.co.uk/demonstration/cask-ale/?cookie_consent=true&autoopen=true",
    ),
    new Module(
      mainModuleName: "Service Suite",
      subModuleName: "The Bartender",
      descriptionParagraphs: [],
      demoUrl: "https://www.flowmodulemanager.co.uk/demonstration/the_bartender/?cookie_consent=true&autoopen=true",
    ),
  ];

  PortfolioModuleSearchList({
    this.searchString,
  });

  List<Widget> listBuilder() {
    List<Widget> children = [];

    this.modules.forEach((Module module) {
      if(
        this.searchString == null ||
        this.searchString.isEmpty ||
        module.mainModuleName.toLowerCase().contains(this.searchString.toLowerCase()) ||
        module.subModuleName.toLowerCase().contains(this.searchString.toLowerCase())
      ) {
        children.add(module);
      }
    });

    return children;
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.symmetric(vertical: 10.0),
      child: new ListView(
        children: this.listBuilder(),
      ),
    );
  }
}

class Module extends StatefulWidget {
  final String mainModuleName;
  final String subModuleName;
  final List<String> descriptionParagraphs;
  final String demoUrl;

  Module({
    this.mainModuleName,
    this.subModuleName,
    this.descriptionParagraphs,
    this.demoUrl,
  });


  @override
  _ModuleState createState() => _ModuleState();
}

class _ModuleState extends State<Module> {

  bool _isExpanded = false;

  List<Widget> _generateParagraph(BuildContext context) {

    List<Widget> children = [];

    if(this.widget.descriptionParagraphs is List){
      this.widget.descriptionParagraphs.forEach((paragraph) {
        children.add(
          new ListTile(
            title: new Text(
              paragraph,
            ),
          )
        );
      });
    }

    children.add( // add bit ad-hoc end padding
      new SizedBox(
        height: 10.0,
      )
    );

    if(this.widget.demoUrl is String) {
      children.add(
        new RaisedButton(
          child: new Text("Demo"),
          onPressed: (){
            Navigator.of(context).push(
              new MaterialPageRoute(builder: (BuildContext newContext) => new LandscapeWebView(url: this.widget.demoUrl, appBarTitle: this.widget.subModuleName,))
            );
          },
        )
      );
    }

    return children;
  }

  @override
  Widget build(BuildContext context) {
    return new Card(
      child: new ExpansionTile(
        title: new ListTile(
          title: new Text(this.widget.subModuleName, textScaleFactor: (this._isExpanded)? 1.2 : 1.0, style: (this._isExpanded)? TextStyle(color: Colors.purpleAccent) : null, ),
          subtitle: new Text(this.widget.mainModuleName),
        ),
        children: this._generateParagraph(context),
        onExpansionChanged: (bool isExpanded) {
          this.setState(() {
            this._isExpanded = isExpanded;
          });
        },
      ),
    );
  }
}
