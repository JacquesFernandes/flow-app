import "package:flutter/material.dart";

class TitledParagraphCard extends StatelessWidget {

  final String title;
  final List<String> paragraphs;

  TitledParagraphCard(
    this.title,
    [this.paragraphs]
  );

  List<Widget> generateCardChildren() {

    List<Widget> children = [];

    children.add(
      new ListTile(
        title: new Text(
          this.title,
          style: new TextStyle(
            color: Colors.purpleAccent,
            fontWeight: FontWeight.bold,
          ),
          textScaleFactor: 1.3,
        ),
      ),
    );

    if(this.paragraphs is List){
      this.paragraphs.forEach((paragraph) {
        children.add(
          new ListTile(
            title: new Text(
              paragraph,
              textScaleFactor: 1.1,
            ),
          )
        );
      });
    }

    children.add( // add bit ad-hoc end padding
      new SizedBox(
        height: 10.0,
      )
    );

    return children;
  }

  @override
  Widget build(BuildContext context) {
    return new Card(
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: this.generateCardChildren(),
      ),
    );
  }
}
