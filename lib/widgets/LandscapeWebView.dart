import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:url_launcher/url_launcher.dart';

class LandscapeWebView extends StatelessWidget {
  final String url;
  final String appBarTitle;

  LandscapeWebView({
    @required this.url,
    this.appBarTitle
  }){
    SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft]);
  }

  Future resetOrientations() {
    Completer completer = new Completer();

    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
    .then((data) {
      return SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown, DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);
    })
    .then((data) {
      completer.complete(null);
    });

    return completer.future;
  }

  void openUrlExternally() {
    canLaunch(this.url)
    .then((bool urlCanLaunch) {
      if(urlCanLaunch){
        launch(this.url)
        .then((data) {
          this.resetOrientations();
        });
      }
      else{
        print("Error: Could not open url: "+this.url.toString());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      child: new WebviewScaffold(
        appBar: new AppBar(
          title: (this.appBarTitle is String)? new Text(this.appBarTitle) : null, // check if appBarTitle is set
          actions: <Widget>[
            new IconButton(
              icon: new Icon(Icons.open_in_new),
              onPressed: this.openUrlExternally,
            )
          ],
        ),
        url: this.url,
        withZoom: true,
      ),
      onWillPop: () {
        this.resetOrientations();
        return new Future(() => true);
      }
    );
  }
}
