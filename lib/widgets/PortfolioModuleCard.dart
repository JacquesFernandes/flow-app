import 'package:flutter/material.dart';

class PortfolioModuleCard extends StatefulWidget {

  final String title;
  final List<String> paragraphs;

  PortfolioModuleCard({ this.title="Test Module", this.paragraphs});

  @override
  _PortfolioModuleCardState createState() => _PortfolioModuleCardState();
}

class _PortfolioModuleCardState extends State<PortfolioModuleCard> {

  bool isExpanded = false;

  List<Widget> generateCardChildren() {

    List<Widget> children = [];

    if(this.widget.paragraphs is List){
      this.widget.paragraphs.forEach((paragraph) {
        children.add(
          new ListTile(
            title: new Text(
              paragraph,
              textScaleFactor: 1.1,
            ),
          )
        );
      });
    }

    children.add( // add bit ad-hoc end padding
      new SizedBox(
        height: 10.0,
      )
    );

    return children;
  }

  @override
  Widget build(BuildContext context) {
    return new Card(
      child: new ExpansionTile(
        onExpansionChanged: (bool hasExpanded) {
          this.setState(() {
            this.isExpanded = hasExpanded;
          });
        },
        title: new Text(
          this.widget.title,
          textScaleFactor: (this.isExpanded)? 1.2 : 1.0,
        ),
        children: this.generateCardChildren(),
      ),
    );
  }
}
