import 'package:flutter/material.dart';

class CertificationsExpansionTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15.0),
      child: new Card(
        child: new ExpansionTile(
          initiallyExpanded: true,
          title: new Text("Certifications", textScaleFactor: 1.2,),
          children: <Widget>[
            new Container(
            margin: EdgeInsets.symmetric(vertical: 15.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new Image.network("https://www.flowhospitalitytraining.co.uk/media/resources/images/complianceLogos/CPD.png", fit: BoxFit.fitHeight,),
                  new Image.network("https://www.flowhospitalitytraining.co.uk/media/resources/images/complianceLogos/CPD.png", fit: BoxFit.fitHeight)
                ],
              ),
            ),
            new Container(
            margin: EdgeInsets.symmetric(vertical: 15.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new Image.network("https://www.flowhospitalitytraining.co.uk/media/resources/images/complianceLogos/CPD.png", fit: BoxFit.fitHeight,),
                  new Image.network("https://www.flowhospitalitytraining.co.uk/media/resources/images/complianceLogos/CPD.png", fit: BoxFit.fitHeight)
                ],
              ),
            ),
            new Container(
            margin: EdgeInsets.symmetric(vertical: 15.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new Image.network("https://www.flowhospitalitytraining.co.uk/media/resources/images/complianceLogos/CPD.png", fit: BoxFit.fitHeight,),
                  new Image.network("https://www.flowhospitalitytraining.co.uk/media/resources/images/complianceLogos/CPD.png", fit: BoxFit.fitHeight)
                ],
              ),
            ),
          ],
        ),
      )
    );
  }
}
