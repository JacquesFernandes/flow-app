import 'package:flutter/material.dart';

import '../pages/home.dart';
import '../pages/portfolio_landing.dart';
import '../pages/flow_zone_landing.dart';
import '../pages/suggestions.dart';
import '../pages/portfolio_module_search.dart';
import './LandscapeWebView.dart';

class FlowDrawer extends StatelessWidget {

  final List<Map<String, dynamic>> _routeObjects = [
    {
      "key": "home",
      "name": "Home",
      "icon": Icons.home,
      "pageBuilder": (BuildContext context) => new Home(),
    },
    {
      "key": "portfolio_landing",
      "name": "Our Portfolio / Modules",
      "icon": Icons.book,
      "pageBuilder": (BuildContext context) => new PortfolioLanding(),
    },
    {
      "key": "module_search",
      "name": "Module Search",
      "icon": Icons.search,
      "pageBuilder": (BuildContext context) => new PortfolioModuleSearch(),
    },
    {
      "key": "flow_zone",
      "name": "The Flow Zone",
      "icon": Icons.ac_unit,
      "pageBuilder": (BuildContext context) => new FlowZoneLanding(),
    },
    {
      "key": "suggestions",
      "name": "Suggestions?",
      "icon": Icons.lightbulb_outline,
      "pageBuilder": (BuildContext context) => new Suggestion(),
    },
    {
      "key": "bauble_blast",
      "name": "Bauble Blast!",
      "icon": Icons.videogame_asset,
      "pageBuilder": (BuildContext context) => new LandscapeWebView(url: "https://www.google.com", appBarTitle: "Bauble Blast!",), // TODO: Set actual url here, instead of the google link
    }
  ];

  List<Widget> _generateDrawerItems(BuildContext context) {

    List<Widget> children = [];
    
    children.add(
      Image.network("https://www.flowhospitalitytraining.co.uk/media/img/logo.png", height: 100.0,)
    );

    children.add(new Divider(color: Colors.white,));

    this._routeObjects.forEach((Map<String, dynamic> route) {
      children.add(
        new ListTile(
          leading: (route['icon'] != null)? new Icon(route['icon']) : null, // in case no icon is provided
          title: new Text(route["name"], textScaleFactor: 1.1,),
          onTap: () {
            Navigator.pop(context); // close the drawer before opening the new route in the next line
            Navigator.push(context, new MaterialPageRoute(builder: route["pageBuilder"]));
          },
        )
      );

      children.add(new Divider(color: Colors.white,));
    });

    return children;
  }

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: new Container(
        child: new ListView(
          children: this._generateDrawerItems(context)
        ),
        margin: EdgeInsets.symmetric(vertical: 10.0),
      )
    );
  }
}
