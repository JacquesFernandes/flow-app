import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import './pages/home.dart';

void main() {
  final FirebaseMessaging messaging = new FirebaseMessaging();
  messaging.getToken()
  .then((token) {
    print("TOKEN: "+token);
  });

  runApp(
    new MaterialApp(
      title: "Flow Hospitality Training",
      theme: new ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.deepPurple,
        accentColor: Colors.purpleAccent, // Changes the "over-scroll" color for ListViews
        buttonTheme: ButtonThemeData(
          buttonColor: Colors.pinkAccent, // Change this to change the default color of any buttons
        )
      ),
      home: new Home(),
    )
  );
}