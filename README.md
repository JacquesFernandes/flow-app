# flow_app

This app was created as a PoC (Proof of Concept) to show that is a very viable option for cross-platform app development using Flutter.

Author Email: jacques.fernandes.1337@gmail.com

## Setup
- To install Flutter and get started, follow the instructions here (https://flutter.io/docs/get-started/install) 
- Try to follow the route which uses Android Studio (there are instructions for using Visual Studio Code as well, but Android Studio is pretty good)
- Clone this repo to the location of your choice
- Enter the "flow_app" directory
- If you're using Android Studio and you installed the Flutter plugin for Android Studio, you can directly run the app from the "run" button
- Instructions for VS Code are also present in the "Get Started" section of (https://flutter.io/docs/get-started/)
- If you have further queries, feel free to reach out via the Author Email. I may take a little while to respond

## Understanding the codebase
- 95% of all the coding will be done in the "/lib" directory
- I've used a convention wherein the main "Screens" are written and placed in the "/lib/pages" directory and any reusable "widgets" are in "/lib/widgets"
- It isn't necessary to follow this convention, you can use your own structure/convention when working on a Flutter app

## Usage Notes

### Regarding the use of Flutter "plugins"
- Much like how Javascript has "node modules", Flutter has "plugins"
- To browse the list of plugins, visit: (https://pub.dartlang.org/flutter/packages)
- All plugins which are used in a Flutter project are listed in the "/pubspec.yaml" file, under "dependencies"
- Instructions on how to add a plugin are also given in the "plugin page" when looking at it (in the "Installing" Tab)

### Regarding Firebase Cloud Messaging (FCM)
- Firebase Cloud Messaging ("Push Notifications") has been added to this project, but will work in debug mode only. If you would like a built debug version of the app, please email me at jacques.fernandes.1337@gmail.com
- The "google-services.json" present in this codebase is my personal one, so if you would like a demo, please reach out to me and ask me to fire a demo notification (with custom content, if you want to see it)
- Said "google-services.json" is present in the "/android/app/" directory
- If you wish to use your own "google-services.json" feel free to replace the contents of the existing file with your own
- The Push Notifications will only show on the device if the user is *not* withing the app at that time (as is the purpose of a notification)
- However, there is a plugin which allows you to generate local notifications (https://pub.dartlang.org/packages/flutter_local_notifications) which can be shown even if the app is open

    

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.io/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.io/docs/cookbook)

For help getting started with Flutter, view our 
[online documentation](https://flutter.io/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.
